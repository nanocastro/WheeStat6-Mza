setwd("/home/pablo/Documentos/WheeStat6-Mza/Quercetina")
library(ggplot2)

#Factores para escalar los datos.
OrigV = 122
OrigI = 402
ResV = 500/140
ResI = 1/180

datos_comercial = read.csv("Datos_Comercial.csv")
datos_comercial$V <- datos_comercial$V*1000 #Pasamos de V a mV

datos_wheestat = read.csv("./DatosWheeStat_raw.txt", sep=" ", header = TRUE, comment.char = "#")
#Escalamos los datos
datos_wheestat$V <- (datos_wheestat$V - OrigV)*ResV
datos_wheestat$I <- (OrigI - datos_wheestat$I)*ResI

#Graficamos
plotIvsV <- ggplot() + geom_point(data=datos_wheestat, aes(x=datos_wheestat$V, datos_wheestat$I, colour='WheeStat') , size=3) +
  geom_line() +
  scale_x_continuous(breaks = seq(from = min(datos_comercial$V)-5, to = max(datos_comercial$V), by = 100)) +
  xlab("V [mV]") +
  ylab("I [uA]")
          
plotIvsV <- plotIvsV + geom_point(data = datos_comercial, aes(x=datos_comercial$V, y=datos_comercial$I, colour='Comercial'), size=3) +
  scale_color_manual("Quercetina", values = c("WheeStat"="blue", "Comercial"="black"))