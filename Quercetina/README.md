# Experimento de validación WheeStat

Se utilizó el potenciostato para medir una solución de quercetina. La técnica utilizada fue voltametría de pulsos diferencial. Los resultados se compararon con los
arrojados por un potenciostato comercial [microSTAT 200 de Dropsens](http://www.dropsens.com/en/pdfs_productos/new_brochures/stat200.pdf).

<img src="/Quercetina/Img/WhatsApp%20Image%202019-03-12%20at%2012.17.39.jpeg" width="400"/> <img src="/Quercetina/Img/20190312_121920.jpg" width="400"/>

## Materiales
- 2 electrodos de grafito (working y counter).
- Parafina
- 1 electrodo de plata (reference).
- solución de Quercetina 10ppm preparada en buffer de fosfato 10mM, pH 2.5.

## Preparación
Para los electrodos de grafito se procedió a sumergir las barras de grafito  durante unos segundos en parafina fundida. El proceso se repitió 3 veces.
Luego se eliminión la parafina del extremo de los electrodos y se pulió de manera mecánica.

Para el electrodo de referencia se se fijó a un extremo del tubo de vidrio provisto por el kit una esfera de porcelana porosa. Luego se mantuvo sumergido en una solución de KCl<sub>sat</sub> durante una semana. El cable de plata se lo recubrió con AgCl mediante electrolisis. Se soldó el cable de plata y se lo introdujo en el interior del tubo de vidrio el cual se llenó con una solución 3 M KCl.

## Resultados

![Quercetina](https://gitlab.com/nanocastro/WheeStat6-Mza/raw/pablo/Quercetina/Img/Plot.png)

### Research Note en PublicLab
https://publiclab.org/notes/nanocastro/04-17-2019/first-experiment-with-the-wheestat-quercetin#c23951