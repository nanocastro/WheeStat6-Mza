---
title: "Primer encuentro: Tecnología libre y Git"
author: "Pablo Cremades"
date: "Marzo 26, 2019"
---

# Tecnología Libre
- [Presentación sobre tecnología libre](https://gitlab.com/pcremades/Taller_Arduino/raw/70cc501c6677ef4aa6851d80cc90cfe4d679230a/TecnoLibre/Seminario_Presentaci%C3%B3n.pdf)
- Licencias: [GPL](https://www.gnu.org/licenses/gpl.html) y [OHL de CERN](https://ohwr.org/project/cernohl/wikis/home)
- Ejemplos de proyectos de hardware abierto bien documentados:
	- [Microscopio OpenFlexture](http://www.docubricks.com/viewer.jsp?id=1967334747077765120)
	- [Fotocompuerta para experimentos de física](https://gitlab.com/pcremades/Taller_Arduino/blob/master/Fotocompuerta/Documentaci%C3%B3n/Construcci%C3%B3n.md)

# Usando Git
 - [Descarga para Windows](https://git-scm.com/download/win)
 - [Video explicativo para empezar](https://www.youtube.com/watch?v=HiXLkL42tMU)
 
## Comandos básicos
 - git clone (URL): clona un repositorio de internet en nuestra compu. Crea una carpeta con el nombre de dicho repositorio. Ejemplo:

```shell
git clone https://gitlab.com/nanocastro/WheeStat6-Mza
```

- git status: muestra el estado en que se encuentra la copia local de nuestro repositorio

- git add (nombre de archivo ó .): agrega uno o varios archivos que se encuentra en la carpeta de trabajo al repositorio para que git los "siga".

- git commit -m "mensaje": crea una instantánea (snapshot) del estado de nuestro repositorio. Un estado al que podemos volver después.

- git push: sube los cambios hechos en nuestra copia local del repositorio al repositorio remoto (GitLab o GitHub)


